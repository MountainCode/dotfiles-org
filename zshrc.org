#+TITLE: ZSH Configuration
#+PROPERTY: header-args:shell :tangle ~/.zshrc

* .NET telemetry opt-out

#+begin_src shell
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export FUNCTIONS_CORE_TOOLS_TELEMETRY_OPTOUT=1
#+end_src

* Default editor

#+begin_src shell
export EDITOR=vim
#+end_src

* Prompt

https://scriptingosx.com/2019/07/moving-to-zsh-06-customizing-the-zsh-prompt/

#+begin_src shell
PROMPT='%(?.%F{green}👍.%F{red}👎) [%D{%F} %D{%L:%M:%S}] %F{cyan}%~'$'\n''%(?.%F{green}.%F{red})>%F{white} '
#+end_src

Conditional substrings allow different behavior based on the value of variables.
~%(x.true-text.false-text)~

| !     | True if the shell is running with privileges.                                                                                                                                                             |
| #     | True if the effective uid of the current process is n.                                                                                                                                                    |
| ?     | True if the exit status of the last command was n.                                                                                                                                                        |
| _     | True if at least n shell constructs were started.                                                                                                                                                         |
| C /   | True if the current absolute path has at least n elements relative to the root directory, hence / is counted as 0 elements.                                                                               |
| c . ~ | True if the current path, with prefix replacement, has at least n elements relative to the root directory, hence / is counted as 0 elements.                                                              |
| D     | True if the month is equal to n (January = 0).                                                                                                                                                            |
| d     | True if the day of the month is equal to n.                                                                                                                                                               |
| e     | True if the evaluation depth is at least n.                                                                                                                                                               |
| g     | True if the effective gid of the current process is n.                                                                                                                                                    |
| j     | True if the number of jobs is at least n.                                                                                                                                                                 |
| L     | True if the SHLVL parameter is at least n.                                                                                                                                                                |
| l     | True if at least n characters have already been printed on the current line. When n is negative, true if at least abs(n) characters remain before the opposite margin (thus the left margin for RPROMPT). |
| S     | True if the SECONDS parameter is at least n.                                                                                                                                                              |
| T     | True if the time in hours is equal to n.                                                                                                                                                                  |
| t     | True if the time in minutes is equal to n.                                                                                                                                                                |
| v     | True if the array psvar has at least n elements.                                                                                                                                                          |
| V     | True if element n of the array psvar is set and non-empty.                                                                                                                                                |
| w     | True if the day of the week is equal to n (Sunday = 0).                                                                                                                                                   |

See section "13.3 Conditional Substrings in Prompts" in the [[http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html][ZSH docs]].

** The right sided prompt

This shows the current branch in the RPROMPT.
#+begin_src shell
RPROMPT=\$vcs_info_msg_0_
#+end_src

* History

#+begin_src shell
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
#+end_src

* Aliases

#+begin_src shell
alias ls='ls --color'
alias tmux='tmux -2'
alias xc='xclip -selection clipboard'
alias runghc='runghc -Wall'
#+end_src

* Functions

** unreddit

#+begin_src shell
function unreddit {
  if [ -z ${1+x} ];
    # If there is no argument, update the clipboard.
    then xc -o | sed 's/reddit\.com/teddit.net/g' | xc;
    else echo $1 | sed 's/reddit\.com/teddit.net/g';
  fi
}
#+end_src

From the man page for bash.
#+begin_src text
-z string
       True if the length of string is zero.
#+end_src

If ~$1~ were an empty string, ~-z $1~ would evaluate to false. We use an
[[https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_06_02][alternate value parameter expansion]] to make ~$1~ become "x" if it's
value is empty making the length 1 so that ~-z~ evaluates to true.

More details in [[https://stackoverflow.com/a/13864829][this Stack Overflow answer]].

*** Examples

#+begin_src shell :tangle nil
if [ -z $var ]; then echo 'var is not set'; else echo "var is set to '${var}'"; fi
#+end_src

#+RESULTS:
: var is not set

#+begin_src shell :tangle nil
var=hello
if [ -z $var ]; then echo 'var is not set'; else echo "var is set to '${var}'"; fi
#+end_src

#+RESULTS:
: var is set to 'hello'

#+begin_src shell :tangle nil
var=''
if [ -z $var ]; then echo 'var is not set'; else echo "var is set to '${var}'"; fi
#+end_src

#+RESULTS:
: var is not set

#+begin_src shell :tangle nil
var=''
echo "${var+x}"
#+end_src

#+RESULTS:
: x

#+begin_src shell :tangle nil
var=''
if [ -z ${var+x} ]; then echo 'var is not set'; else echo "var is set to '${var}'"; fi
#+end_src

#+RESULTS:
: var is set to ''

* Nix

#+begin_src shell
. ~/.nix-profile/etc/profile.d/nix.sh
#+end_src

* GPG

#+begin_src shell
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
#+end_src

* fzf

#+begin_src shell
export FZF_DEFAULT_OPTS='--height 40% --reverse'
export FZF_DEFAULT_COMMAND='ag -g ""'
#+end_src

* Disorganized

#+begin_src shell
# Lines configured by zsh-newuser-install
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/chris/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

export PATH="${HOME}/bin:${PATH}"

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
zstyle ':vcs_info:git:*' formats '%F{13}%b %u%f'
zstyle ':vcs_info:*' enable git
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

export PATH="$HOME/.local/bin:$PATH"
#+end_src

* Unused                                                            :ARCHIVE:

** Aliases

#+begin_src shell
alias racket='racket -il readline'
alias stack='stack --nix --no-strip'
#+end_src

** NPM

I use project local NPM or Nix.

#+begin_src shell
NPM_PACKAGES="${HOME}/.npm-packages"
PATH="$NPM_PACKAGES/bin:$PATH"
#+end_src

** Ruby

** Rust

#+begin_src shell
#+end_src

